/* Copyright (C) 2020-2023 Stephan Kreutzer
 *
 * This file is part of BAC.
 *
 * BAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * BAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with BAC. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/Segment.h
 * @author Stephan Kreutzer
 * @since 2020-10-24
 */

#ifndef _BAC_SEGMENT_H
#define _BAC_SEGMENT_H


#include <string>
#include <vector>
#include <memory>
#include <list>
#include <map>


namespace bac
{

class Segment
{
public:
    class Hole;

public:
    Segment(const std::string& strString);
    Segment(const std::string& strString, std::unique_ptr<std::map<std::size_t, Segment::Hole>>& pHoles);

public:
    std::string GetString(const std::vector<std::unique_ptr<std::string>>& aArguments);
    const std::string& GetStringOriginal();
    int SetString(const std::string& strString);

    int DrillHoles(const std::list<std::unique_ptr<std::string>>& aStrings);
    int GetHoles(std::unique_ptr<std::map<std::size_t, Segment::Hole>>& pHoles);

    int CallRestore();
    int CallN(const std::size_t& nCharacterCount, std::unique_ptr<std::string>& pResult);
    int CallSegment(std::unique_ptr<std::string>& pResult);
    int CallCharacter(std::unique_ptr<std::string>& pResult);
    int CallIn(const std::string& strNeedle, std::unique_ptr<std::string>& pResult);

public:
    class Hole
    {
    public:
        Hole(const std::size_t& nPosition, const std::size_t& nLength, const std::size_t& nHoleNumber);

    public:
        const std::size_t& GetPosition();
        const std::size_t& GetLength();
        const std::size_t& GetHoleNumber();

    protected:
        const std::size_t m_nPosition = std::string::npos;
        const std::size_t m_nLength = std::string::npos;
        const std::size_t m_nHoleNumber = -1;
    };

protected:
    std::string m_strString;
    std::size_t m_nHoleNumberCount = 0U;
    // <character position, hole info>
    std::unique_ptr<std::map<std::size_t, Segment::Hole>> m_pHoles = nullptr;
    // Reliance on internal state prevents recursive/nested use!
    std::size_t m_nFormPointer = 0U;

};

}

#endif
