/* Copyright (C) 2020-2023 Stephan Kreutzer
 *
 * This file is part of BAC.
 *
 * BAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * BAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with BAC. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/Segment.cpp
 * @author Stephan Kreutzer
 * @since 2020-10-24
 */


#include <sstream>
#include "Segment.h"


namespace bac
{

Segment::Segment(const std::string& strString):
  m_strString(strString)
{
    if (strString.empty() == true)
    {
        throw new std::invalid_argument("Segment::Segment(): Empty string passed.");
    }
}

Segment::Segment(const std::string& strString,
                 std::unique_ptr<std::map<std::size_t, Segment::Hole>>& pHoles):
  m_strString(strString),
  m_pHoles(std::move(pHoles))
{
    if (strString.empty() == true)
    {
        throw new std::invalid_argument("Segment::Segment(): Empty string passed.");
    }
}

std::string Segment::GetString(const std::vector<std::unique_ptr<std::string>>& aArguments)
{
    if (m_pHoles == nullptr)
    {
        return m_strString;
    }

    std::stringstream strResult;
    std::size_t nStart = 0U;

    for (std::map<std::size_t, Segment::Hole>::iterator iter = m_pHoles->begin();
         iter != m_pHoles->end();
         iter++)
    {
        strResult << m_strString.substr(nStart, iter->first - nStart);

        const std::size_t& nHoleNumber = iter->second.GetHoleNumber();

        if (nHoleNumber < aArguments.size())
        {
            strResult << *(aArguments.at(nHoleNumber));
        }

        nStart = iter->first + iter->second.GetLength();
    }

    strResult << m_strString.substr(nStart, std::string::npos);

    return strResult.str();
}

const std::string& Segment::GetStringOriginal()
{
    return m_strString;
}

int Segment::SetString(const std::string& strString)
{
    m_strString = strString;

    if (m_pHoles != nullptr)
    {
        m_pHoles.reset(nullptr);
    }

    return 0;
}

int Segment::DrillHoles(const std::list<std::unique_ptr<std::string>>& aStrings)
{
    if (aStrings.empty() == true)
    {
        throw new std::invalid_argument("Segment::DrillHoles(): No arguments.");
    }

    if (m_pHoles == nullptr)
    {
        m_pHoles.reset(new std::map<std::size_t, Segment::Hole>());
    }

    for (std::list<std::unique_ptr<std::string>>::const_iterator iter = aStrings.begin();
         iter != aStrings.end();
         iter++)
    {
        if ((*iter)->empty() == true)
        {
            throw new std::invalid_argument("Segment::DrillHoles(): Empty string passed.");
        }

        bool bFound = false;
        std::size_t nPos = m_strString.find(**iter);

        if (nPos == std::string::npos)
        {
            std::stringstream aMessage;
            aMessage << "String \"" << **iter << "\" not found in segment \"" << m_strString << "\".";
            throw new std::runtime_error(aMessage.str());
        }

        do
        {
            {
                // Identify + handle overlapping.

                std::map<std::size_t, Segment::Hole>::iterator iterAfter = m_pHoles->upper_bound(nPos);

                if (iterAfter != m_pHoles->end())
                {
                    if ((nPos + (*iter)->length()) > iterAfter->first)
                    {
                        nPos = m_strString.find(**iter, iterAfter->first + iterAfter->second.GetLength());
                        continue;
                    }
                }

                std::map<std::size_t, Segment::Hole>::iterator iterBeforeOrOn = iterAfter;

                if (iterAfter != m_pHoles->end())
                {
                    if (iterBeforeOrOn != m_pHoles->begin())
                    {
                        --iterBeforeOrOn;
                    }
                    else
                    {
                        // iterBeforeOrOn is still iterAfter, which was after nPos and
                        // is also the begin(), so no checking on iterBeforeOrOn needed.
                        iterBeforeOrOn = m_pHoles->end();
                    }
                }
                else
                {
                    // m_pHoles->lower_bound(nPos) does not provide before, only on/after,
                    // because it's expensive to determine a before in a map based on key.
                    /** @todo This can likely be optimized by having iterBeforeOrOn in the
                      * outer iter loop, basically keeping the last iterBeforeOrOn, and
                      * progressing it along towards the next found nPos. */

                    std::map<std::size_t, Segment::Hole>::iterator iterTemp = m_pHoles->begin();
                    iterBeforeOrOn = iterTemp;

                    while (iterTemp != m_pHoles->end())
                    {
                        if (iterTemp->first < nPos)
                        {
                            iterBeforeOrOn = iterTemp;
                            ++iterTemp;
                        }
                        else
                        {
                            break;
                        }
                    }
                }

                if (iterBeforeOrOn != m_pHoles->end())
                {
                    if ((iterBeforeOrOn->first + iterBeforeOrOn->second.GetLength()) > nPos)
                    {
                        nPos = m_strString.find(**iter, iterBeforeOrOn->first + iterBeforeOrOn->second.GetLength());
                        continue;
                    }
                }
            }

            m_pHoles->insert(std::pair<std::size_t, Segment::Hole>(nPos, Segment::Hole(nPos, (*iter)->length(), m_nHoleNumberCount)));
            bFound = true;

            if (m_nFormPointer > nPos &&
                m_nFormPointer <= (nPos + (*iter)->length()))
            {
                m_nFormPointer = nPos;
            }

            nPos = m_strString.find(**iter, nPos + (*iter)->length());

        } while (nPos != std::string::npos);

        if (bFound == false)
        {
            std::vector<std::unique_ptr<std::string>> aArgumentsEmpty;
            std::string strStringWithHoles = GetString(aArgumentsEmpty);

            std::stringstream aMessage;
            aMessage << "String \"" << **iter << "\" not found in segment \"" << strStringWithHoles << "\" (with " << m_pHoles->size() << " holes).";
            throw new std::runtime_error(aMessage.str());
        }

        ++m_nHoleNumberCount;
    }

    return 0;
}

/**
 * @todo Could work with shared_ptr<> for m_pHoles/pHoles here?
 */
int Segment::GetHoles(std::unique_ptr<std::map<std::size_t, Segment::Hole>>& pHoles)
{
    if (pHoles != nullptr)
    {
        throw new std::invalid_argument("Segment::GetHoles() with pHoles != nullptr.");
    }

    if (m_pHoles == nullptr)
    {
        return 1;
    }

    if (m_pHoles->empty() == true)
    {
        return 1;
    }

    pHoles = std::unique_ptr<std::map<std::size_t, Segment::Hole>>(new std::map<std::size_t, Segment::Hole>);

    pHoles->insert(m_pHoles->begin(), m_pHoles->end());

    return 0;
}

int Segment::CallRestore()
{
    m_nFormPointer = 0UL;
    return 0;
}

int Segment::CallN(const std::size_t& nCharacterCount, std::unique_ptr<std::string>& pResult)
{
    if (pResult != nullptr)
    {
        throw new std::invalid_argument("Segment::CallN() with pResult != nullptr.");
    }

    std::string strString;

    {
        std::vector<std::unique_ptr<std::string>> aArgumentsEmpty;
        strString = GetString(aArgumentsEmpty);
    }

    if (m_nFormPointer >= strString.length())
    {
        return 1;
    }

    pResult.reset(new std::string);

    *pResult = strString.substr(m_nFormPointer, nCharacterCount);

    m_nFormPointer += nCharacterCount;

    return 0;
}

int Segment::CallSegment(std::unique_ptr<std::string>& pResult)
{
    if (pResult != nullptr)
    {
        throw new std::invalid_argument("Segment::CallSegment() with pResult != nullptr.");
    }

    std::string strString;

    {
        std::vector<std::unique_ptr<std::string>> aArgumentsEmpty;
        strString = GetString(aArgumentsEmpty);
    }

    if (m_nFormPointer >= strString.length())
    {
        return 1;
    }

    if (m_pHoles != nullptr)
    {
        if (m_pHoles->empty() != true)
        {
            /** @todo Could this too be implemented with std::map<>::lower_bound()? */

            std::size_t nPosOffset(0U);
            std::size_t nNext(std::string::npos);

            for (std::map<std::size_t, Segment::Hole>::iterator iter = m_pHoles->begin();
                iter != m_pHoles->end();
                iter++)
            {
                if (m_nFormPointer > iter->first &&
                    m_nFormPointer < (iter->first + iter->second.GetLength()))
                {
                    // Form pointer can't be inside a hole (in case new holes
                    // got drilled since the last form pointer operation).
                    // But maybe this case here is impossible, as DrillHoles()
                    // already takes care of it?

                    m_nFormPointer = iter->first + iter->second.GetLength();
                }

                std::size_t nPosTarget = iter->first - nPosOffset;
                nPosOffset += iter->second.GetLength();

                if (nPosTarget > m_nFormPointer)
                {
                    nNext = nPosTarget;
                    break;
                }
            }

            pResult.reset(new std::string);

            *pResult = strString.substr(m_nFormPointer, (nNext != std::string::npos ? nNext - m_nFormPointer : std::string::npos));

            m_nFormPointer = nNext;

            return 0;
        }
        else
        {
            pResult.reset(new std::string);

            *pResult = strString.substr(m_nFormPointer, std::string::npos);

            m_nFormPointer = std::string::npos;

            return 0;
        }
    }
    else
    {
        pResult.reset(new std::string);

        *pResult = strString.substr(m_nFormPointer, std::string::npos);

        m_nFormPointer = std::string::npos;

        return 0;
    }
}

int Segment::CallCharacter(std::unique_ptr<std::string>& pResult)
{
    if (pResult != nullptr)
    {
        throw new std::invalid_argument("Segment::CallCharacter() with pResult != nullptr.");
    }

    std::string strString;

    {
        std::vector<std::unique_ptr<std::string>> aArgumentsEmpty;
        strString = GetString(aArgumentsEmpty);
    }

    if (m_nFormPointer >= strString.length())
    {
        return 1;
    }

    pResult.reset(new std::string);

    *pResult = strString.substr(m_nFormPointer, 1U);

    m_nFormPointer += 1U;

    return 0;
}

int Segment::CallIn(const std::string& strNeedle, std::unique_ptr<std::string>& pResult)
{
    if (pResult != nullptr)
    {
        throw new std::invalid_argument("Segment::CallIn() with pResult != nullptr.");
    }

    std::string strString;

    {
        std::vector<std::unique_ptr<std::string>> aArgumentsEmpty;
        strString = GetString(aArgumentsEmpty);
    }

    if (m_nFormPointer >= strString.length())
    {
        return 1;
    }

    std::size_t nPos = strString.find(strNeedle, m_nFormPointer);

    if (nPos != std::string::npos)
    {
        m_nFormPointer = nPos;
    }
    else
    {
        return 1;
    }

    return 0;
}

Segment::Hole::Hole(const std::size_t& nPosition, const std::size_t& nLength, const std::size_t& nHoleNumber):
  m_nPosition(nPosition), m_nLength(nLength), m_nHoleNumber(nHoleNumber)
{
    
}

const std::size_t& Segment::Hole::GetPosition()
{
    return m_nPosition;
}

const std::size_t& Segment::Hole::GetLength()
{
    return m_nLength;
}

const std::size_t& Segment::Hole::GetHoleNumber()
{
    return m_nHoleNumber;
}

}
