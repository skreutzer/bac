/* Copyright (C) 2020-2023 Stephan Kreutzer
 *
 * This file is part of BAC.
 *
 * BAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * BAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with BAC. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/Segment.cpp
 * @author Stephan Kreutzer
 * @since 2020-10-24
 */


#include <sstream>
#include "Segment.h"


namespace bac
{

Segment::Segment(const std::string& strString):
  m_strString(strString)
{
    if (strString.empty() == true)
    {
        throw new std::invalid_argument("Segment::Segment(): Empty string passed.");
    }
}

std::string Segment::GetString(const std::vector<std::unique_ptr<std::string>>& aArguments)
{
    if (m_pHoles == nullptr)
    {
        return m_strString;
    }

    std::stringstream strResult;
    std::size_t nStart = 0U;

    for (std::map<std::size_t, Segment::Hole>::iterator iter = m_pHoles->begin();
         iter != m_pHoles->end();
         iter++)
    {
        strResult << m_strString.substr(nStart, iter->first - nStart);

        const std::size_t& nHoleNumber = iter->second.GetHoleNumber();

        if (nHoleNumber < aArguments.size())
        {
            strResult << *(aArguments.at(nHoleNumber));
        }

        nStart = iter->first + iter->second.GetLength();
    }

    strResult << m_strString.substr(nStart, std::string::npos);

    return strResult.str();
}

int Segment::SetString(const std::string& strString)
{
    m_strString = strString;

    if (m_pHoles != nullptr)
    {
        m_pHoles.reset(nullptr);
    }

    return 0;
}

int Segment::DrillHoles(const std::list<std::unique_ptr<std::string>>& aStrings)
{
    if (aStrings.empty() == true)
    {
        throw new std::invalid_argument("Segment::DrillHoles(): No arguments.");
    }

    if (m_pHoles == nullptr)
    {
        m_pHoles.reset(new std::map<std::size_t, Segment::Hole>());
    }

    for (std::list<std::unique_ptr<std::string>>::const_iterator iter = aStrings.begin();
         iter != aStrings.end();
         iter++)
    {
        if ((*iter)->empty() == true)
        {
            throw new std::invalid_argument("Segment::DrillHoles(): Empty string passed.");
        }

        bool bFound = false;
        std::size_t nPos = m_strString.find(**iter);

        if (nPos == std::string::npos)
        {
            std::stringstream aMessage;
            aMessage << "String \"" << **iter << "\" not found in segment \"" << m_strString << "\".";
            throw new std::runtime_error(aMessage.str());
        }

        do
        {
            {
                // Identify + handle overlapping.

                std::map<std::size_t, Segment::Hole>::iterator iterAfter = m_pHoles->upper_bound(nPos);

                if (iterAfter != m_pHoles->end())
                {
                    if ((nPos + (*iter)->length()) > iterAfter->first)
                    {
                        nPos = m_strString.find(**iter, iterAfter->first + iterAfter->second.GetLength());
                        continue;
                    }
                }

                std::map<std::size_t, Segment::Hole>::iterator iterBeforeOrOn = iterAfter;

                if (iterAfter != m_pHoles->end())
                {
                    if (iterBeforeOrOn != m_pHoles->begin())
                    {
                        --iterBeforeOrOn;
                    }
                    else
                    {
                        // iterBeforeOrOn is still iterAfter, which was after nPos and
                        // is also the begin(), so no checking on iterBeforeOrOn needed.
                        iterBeforeOrOn = m_pHoles->end();
                    }
                }
                else
                {
                    // m_pHoles->lower_bound(nPos) does not provide before, only on/after,
                    // because it's expensive to determine a before in a map based on key.
                    /** @todo This can likely be optimized by having iterBeforeOrOn in the
                      * outer iter loop, basically keeping the last iterBeforeOrOn, and
                      * progressing it along towards the next found nPos. */

                    std::map<std::size_t, Segment::Hole>::iterator iterTemp = m_pHoles->begin();
                    iterBeforeOrOn = iterTemp;

                    while (iterTemp != m_pHoles->end())
                    {
                        if (iterTemp->first < nPos)
                        {
                            iterBeforeOrOn = iterTemp;
                            ++iterTemp;
                        }
                        else
                        {
                            break;
                        }
                    }
                }

                if (iterBeforeOrOn != m_pHoles->end())
                {
                    if ((iterBeforeOrOn->first + iterBeforeOrOn->second.GetLength()) > nPos)
                    {
                        nPos = m_strString.find(**iter, iterBeforeOrOn->first + iterBeforeOrOn->second.GetLength());
                        continue;
                    }
                }
            }

            m_pHoles->insert(std::pair<std::size_t, Segment::Hole>(nPos, Segment::Hole(nPos, (*iter)->length(), m_nHoleNumberCount)));
            bFound = true;

            nPos = m_strString.find(**iter, nPos + (*iter)->length());

        } while (nPos != std::string::npos);

        if (bFound == false)
        {
            std::vector<std::unique_ptr<std::string>> aArgumentsEmpty;
            std::string strStringWithHoles = GetString(aArgumentsEmpty);

            std::stringstream aMessage;
            aMessage << "String \"" << **iter << "\" not found in segment \"" << strStringWithHoles << "\" (with " << m_pHoles->size() << " holes).";
            throw new std::runtime_error(aMessage.str());
        }

        ++m_nHoleNumberCount;
    }

    return 0;
}

Segment::Hole::Hole(const std::size_t& nPosition, const std::size_t& nLength, const std::size_t& nHoleNumber):
  m_nPosition(nPosition), m_nLength(nLength), m_nHoleNumber(nHoleNumber)
{
    
}

const std::size_t& Segment::Hole::GetPosition()
{
    return m_nPosition;
}

const std::size_t& Segment::Hole::GetLength()
{
    return m_nLength;
}

const std::size_t& Segment::Hole::GetHoleNumber()
{
    return m_nHoleNumber;
}

}
