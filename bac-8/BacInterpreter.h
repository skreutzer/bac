/* Copyright (C) 2020-2021 Stephan Kreutzer
 *
 * This file is part of BAC.
 *
 * BAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * BAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with BAC. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/BacInterpreter.h
 * @author Stephan Kreutzer
 * @since 2020-09-23
 */

#ifndef _BAC_BACINTERPRETER_H
#define _BAC_BACINTERPRETER_H

#include <string>
#include <locale>
#include <memory>
#include <list>
#include <map>
#include <stdexcept>
#include "InputStreamInterface.h"
#include "OutputStreamInterface.h"
#include "Segment.h"

namespace bac
{

class BacInterpreter
{
public:
    BacInterpreter(InputStreamInterface& aInputStream,
                   OutputStreamInterface& aOutputStream);
    ~BacInterpreter();

public:
    int Read(std::unique_ptr<std::string>& pResult);

protected:
    int HandleCommand(InputStreamInterface& aInputStream,
                      std::unique_ptr<std::string>& pResult,
                      bool bIsOneOff);
    int HandleAttribute(const char& cFirstByte,
                        InputStreamInterface& aInputStream,
                        std::unique_ptr<std::string>& pResult);

protected:
    int ExecuteCommand(const std::string& strCommand,
                       const std::list<std::unique_ptr<std::string>>& aArguments,
                       std::unique_ptr<std::string>& pResult);

protected:
    char ConsumeWhitespace(InputStreamInterface& aInputStream);

protected:
    std::map<std::string, int (*)(const std::list<std::unique_ptr<std::string>>&, std::unique_ptr<std::string>&)> m_aFunctions;
    std::map<std::string, std::unique_ptr<Segment>> m_aStandby;

protected:
    InputStreamInterface& m_aInputStream;
    OutputStreamInterface& m_aOutputStream;
    std::locale m_aLocale;
    char m_cMetaCharacter;

};

}

#endif
