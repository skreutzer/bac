/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of BAC.
 *
 * BAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * BAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with BAC. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/InputStreamCurses.cpp
 * @author Stephan Kreutzer
 * @since 2021-06-14
 */


#include "InputStreamCurses.h"
#include <stdexcept>
#include <curses.h>


namespace bac
{

InputStreamCurses::InputStreamCurses()
{

}

InputStreamCurses::~InputStreamCurses()
{

}

int InputStreamCurses::get(char& cCharacter)
{
    if (m_bIsInErr == true)
    {
        throw new std::runtime_error("Call of InputStreamCurses::get() after entering InputStreamCurses::eof() state.");
    }

    int nCharacter(getch());

    if (nCharacter == ERR)
    {
        cCharacter = '\0';
        m_bIsInErr = true;

        return 1;
    }

    cCharacter = static_cast<char>(nCharacter);

    return 0;
}

bool InputStreamCurses::eof()
{
    return m_bIsInErr;
}

bool InputStreamCurses::bad()
{
    return m_bIsInErr;
}

}
