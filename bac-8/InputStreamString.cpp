/* Copyright (C) 2020-2021 Stephan Kreutzer
 *
 * This file is part of BAC.
 *
 * BAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * BAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with BAC. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/InputStreamString.cpp
 * @author Stephan Kreutzer
 * @since 2021-06-13
 */


#include "InputStreamString.h"
#include <stdexcept>


namespace bac
{

InputStreamString::InputStreamString(const std::string& strString):
  m_strString(strString),
  m_nSize(strString.size()),
  m_nCurrentPosition(0U),
  m_bIsEof(false)
{

}

InputStreamString::~InputStreamString()
{

}

int InputStreamString::get(char& cCharacter)
{
    if (m_nCurrentPosition >= m_nSize)
    {
        m_bIsEof = true;

        cCharacter = '\0';
        return std::char_traits<char>::eof();
    }

    cCharacter = m_strString.at(m_nCurrentPosition);
    m_nCurrentPosition += 1U;

    return 0;
}

bool InputStreamString::eof()
{
    return m_bIsEof;
}

bool InputStreamString::bad()
{
    return false;
}

}
