/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of BAC.
 *
 * BAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * BAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with BAC. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/OutputStreamCurses.cpp
 * @author Stephan Kreutzer
 * @since 2021-06-19
 */


#include "OutputStreamCurses.h"
#include <memory>
#include <cstring>
#include <curses.h>


namespace bac
{

OutputStreamCurses::OutputStreamCurses()
{

}

OutputStreamCurses::~OutputStreamCurses()
{

}

int OutputStreamCurses::print(const std::string& str)
{
    // Make str.c_str() stable as
    // unknown/non-quaranteed which operations
    // std::strncpy() will perform on it.
    std::unique_ptr<char> pStr;
    std::size_t nLength(str.size());
    pStr.reset(new char[nLength + 1U]);

    if (nLength > 0U)
    {
        std::strncpy(pStr.get(), str.c_str(), nLength);
    }

    pStr.get()[nLength] = '\0';

    printw(pStr.get());
    refresh();

    return 0;
}

}
