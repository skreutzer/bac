/* Copyright (C) 2020-2023 Stephan Kreutzer
 *
 * This file is part of BAC.
 *
 * BAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * BAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with BAC. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/bac.cpp
 * @brief Demo program.
 * @author Stephan Kreutzer
 * @since 2020-09-23
 */

#include "BacInterpreter.h"
#include "InputStreamInterface.h"
#include "InputStreamStd.h"
#include "InputStreamCurses.h"
#include "OutputStreamStd.h"
#include "OutputStreamCurses.h"
#include <memory>
#include <iostream>
#include <fstream>
#include <curses.h>
#include <cstring>


int Run(bac::InputStreamInterface& aInputStream,
        bac::OutputStreamInterface& aOutputStream,
        std::unique_ptr<std::string>& pResult);


int main(int argc, char* argv[])
{
    std::string strCopyrightNotice("BAC Copyright (C) 2020-2023 Stephan Kreutzer\n"
                                   "This program comes with ABSOLUTELY NO WARRANTY.\n"
                                   "This is free software, and you are welcome to redistribute it\n"
                                   "under certain conditions. See the GNU Affero General Public License 3\n"
                                   "or any later version for details. Also, see the source code repository\n"
                                   "https://gitlab.com/skreutzer/bac/ and the project\n"
                                   "website https://hypertext-systems.org.\n");

    int nResult = 0;

    if (argc >= 2)
    {
        std::cout << strCopyrightNotice
                  << std::endl;

        bac::OutputStreamStd aOutputStream;
        std::unique_ptr<std::ifstream> pInputStream = nullptr;

        try
        {
            pInputStream = std::unique_ptr<std::ifstream>(new std::ifstream);
            pInputStream->open(argv[1], std::ios::in | std::ios::binary);

            if (pInputStream->is_open() == true)
            {
                bac::InputStreamStd aInputStream(*pInputStream);
                std::unique_ptr<std::string> pResult;

                Run(aInputStream, aOutputStream, pResult);

                pInputStream->close();

                std::cout << *pResult << std::endl;
            }
            else
            {
                std::cout << "Couldn't open input file \"" << argv[1] << "\"." << std::endl;

                nResult = 1;
            }
        }
        catch (std::exception* pException)
        {
            std::cerr << "Exception: " << pException->what() << std::endl;

            if (pInputStream != nullptr)
            {
                if (pInputStream->is_open() == true)
                {
                    pInputStream->close();
                }
            }

            nResult = 1;
        }
    }
    else
    {
        initscr();

        {
            // Make strCopyrightNotice.c_str() stable as
            // unknown/non-quaranteed which operations
            // std::strncpy() will perform on it.
            std::unique_ptr<char> pStrCopyrightNotice;
            std::size_t nLength(strCopyrightNotice.size());
            pStrCopyrightNotice.reset(new char[nLength + 1U]);

            if (nLength > 0U)
            {
                std::strncpy(pStrCopyrightNotice.get(), strCopyrightNotice.c_str(), nLength);
            }

            pStrCopyrightNotice.get()[nLength] = '\0';

            printw(pStrCopyrightNotice.get());
            refresh();
        }

        printw("\nPlease enter your program:\n");
        refresh();

        bac::OutputStreamCurses aOutputStream;

        try
        {
            bac::InputStreamCurses aInputStream;
            std::unique_ptr<std::string> pResult;

            Run(aInputStream, aOutputStream, pResult);

            printw("\n\n%s\n", pResult->c_str());
            refresh();
        }
        catch (std::exception* pException)
        {
            printw("Exception: %s\n", pException->what());
            refresh();

            nResult = 1;
        }

        endwin();
    }

    return nResult;
}

int Run(bac::InputStreamInterface& aInputStream,
        bac::OutputStreamInterface& aOutputStream,
        std::unique_ptr<std::string>& pResult)
{
    if (pResult != nullptr)
    {
        throw new std::invalid_argument("Run() with pResult != nullptr.");
    }

    bac::BacInterpreter aInterpreter(aInputStream,
                                     aOutputStream);

    aInterpreter.Read(pResult);

    return 0;
}
