/* Copyright (C) 2020-2023 Stephan Kreutzer
 *
 * This file is part of BAC.
 *
 * BAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * BAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with BAC. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/BacInterpreter.cpp
 * @attention Commands "SB" and "FB" are a security risk!
 * @todo Alternative: Reverse Polish notation if reading forward.
 * @author Stephan Kreutzer
 * @since 2020-09-23
 */

#include "BacInterpreter.h"
#include "BacImplementation.h"
#include "InputStreamString.h"
#include <sstream>
#include <iomanip>
#include <fstream>


typedef int (*BacFunction)(const std::list<std::unique_ptr<std::string>>&, std::unique_ptr<std::string>&);

long ParseNumber(const std::string& strInput);


namespace bac
{

BacInterpreter::BacInterpreter(InputStreamInterface& aInputStream,
                               OutputStreamInterface& aOutputStream):
  m_aInputStream(aInputStream),
  m_aOutputStream(aOutputStream),
  m_cMetaCharacter('\n')
{
    m_aFunctions.insert(std::pair<std::string, BacFunction>("AD", AD));
    m_aFunctions.insert(std::pair<std::string, BacFunction>("SU", SU));
    m_aFunctions.insert(std::pair<std::string, BacFunction>("ML", ML));
    m_aFunctions.insert(std::pair<std::string, BacFunction>("DV", DV));
    m_aFunctions.insert(std::pair<std::string, BacFunction>("EQ", EQ));

    //m_aStandby.insert(std::pair<std::string, std::unique_ptr<Segment>>("REPL", new Segment("#(DS,REPL,(#(DS,INPUT,#(RS))#(INPUT)#(DD,INPUT)#(REPL)))")));
}

BacInterpreter::~BacInterpreter()
{

}

int BacInterpreter::Read(std::unique_ptr<std::string>& pResult)
{
    if (pResult != nullptr)
    {
        throw new std::invalid_argument("BacInterpreter::Read() with pResult != nullptr.");
    }

    char cByte = '\0';

    do
    {
        m_aInputStream.get(cByte);

        if (m_aInputStream.eof() == true)
        {
            if (pResult == nullptr)
            {
                pResult.reset(new std::string);
            }

            return 0;
        }

        if (m_aInputStream.bad() == true)
        {
            throw new std::runtime_error("Stream is bad.");
        }

        if (cByte == '#')
        {
            std::unique_ptr<std::string> pValue(nullptr);

            HandleCommand(m_aInputStream, pValue, false);

            if (pValue == nullptr)
            {
                throw new std::runtime_error("BacInterpreter::HandleCommand() returned with nullptr for its pResult parameter.");
            }

            if (pResult == nullptr)
            {
                pResult = std::move(pValue);
            }
            else
            {
                *pResult += *pValue;
            }
        }
        else
        {
            int nByte(cByte);
            std::stringstream aMessage;
            aMessage << "Unexpected character '" << cByte << "' (0x"
                     << std::hex << std::uppercase << std::right << std::setfill('0') << std::setw(2) << nByte
                     << ").";
            throw new std::runtime_error(aMessage.str());
        }

    } while (true);
}

int BacInterpreter::HandleCommand(InputStreamInterface& aInputStream, std::unique_ptr<std::string>& pResult, bool bIsOneOff)
{
    if (pResult != nullptr)
    {
        throw new std::invalid_argument("BacInterpreter::HandleCommand() with pResult != nullptr.");
    }

    char cByte = '\0';

    aInputStream.get(cByte);

    if (aInputStream.eof() == true)
    {
        throw new std::runtime_error("Unexpected end of stream at the start of a command.");
    }

    if (aInputStream.bad() == true)
    {
        throw new std::runtime_error("Stream is bad.");
    }

    if (cByte == '#')
    {
        return HandleCommand(aInputStream, pResult, true);
    }

    if (cByte != '(')
    {
        int nByte(cByte);
        std::stringstream aMessage;
        aMessage << "Unexpected character '" << cByte << "' (0x"
                 << std::hex << std::uppercase << std::right << std::setfill('0') << std::setw(2) << nByte
                 << ") at the start of a command.";
        throw new std::runtime_error(aMessage.str());
    }

    std::stringstream strCommand;

    do
    {
        aInputStream.get(cByte);

        if (aInputStream.eof() == true)
        {
            throw new std::runtime_error("Unexpected end of stream while attempting to read the command name.");
        }

        if (aInputStream.bad() == true)
        {
            throw new std::runtime_error("Stream is bad.");
        }

        if (std::isalnum(cByte, m_aLocale) == true)
        {
            strCommand << cByte;
        }
        else if (cByte == ',')
        {
            std::string strCommandString = strCommand.str();

            if (strCommandString.empty() == true)
            {
                throw new std::runtime_error("Command started without name.");
            }

            std::list<std::unique_ptr<std::string>> aArguments;

            do
            {
                cByte = ConsumeWhitespace(aInputStream);

                if (cByte == '\0')
                {
                    throw new std::runtime_error("Unexpected end of stream while consuming whitespace in front of arguments.");
                }

                std::unique_ptr<std::string> pValue(nullptr);

                int nAttributeResult = HandleAttribute(cByte, aInputStream, pValue);

                if (nAttributeResult == 0)
                {
                    aArguments.push_back(std::move(pValue));
                    continue;
                }
                else if (nAttributeResult == 1)
                {
                    aArguments.push_back(std::move(pValue));
                    break;
                }
                else
                {
                    std::stringstream aMessage;
                    aMessage << "Unexpected return value " << nAttributeResult << " from BacInterpreter::HandleAttribute().";
                    throw new std::runtime_error(aMessage.str());
                }

            } while (true);

            if (aArguments.empty() == true)
            {
                throw new std::runtime_error("Command without arguments.");
            }

            int nResult = ExecuteCommand(strCommandString, aArguments, pResult);

            if (nResult == 0)
            {
                return 0;
            }
            else if (nResult == 1)
            {
                if (pResult->empty() == true)
                {
                    return 0;
                }

                if (pResult->at(0) != '#' ||
                    bIsOneOff == true)
                {
                    return 0;
                }

                InputStreamString aAttribute(pResult->substr(1));

                std::unique_ptr<std::string> pValue(nullptr);

                int nAttributeResult = HandleAttribute(pResult->at(0), aAttribute, pValue);

                if (nAttributeResult == 0)
                {
                    throw new std::runtime_error("Use of BacInterpreter::HandleAttribute() to call a command failed.");
                }
                else if (nAttributeResult == 1)
                {
                    pResult = std::move(pValue);
                    return 0;
                }
                else if (nAttributeResult == 2)
                {
                    // Was a call, pseudo "attribute" handling.
                    pResult = std::move(pValue);
                    return 0;
                }
                else
                {
                    std::stringstream aMessage;
                    aMessage << "Unexpected return value " << nAttributeResult << " from BacInterpreter::HandleAttribute().";
                    throw new std::runtime_error(aMessage.str());
                }
            }
            else
            {
                std::stringstream aMessage;
                aMessage << "Unexpected return value " << nResult << " from BacInterpreter::ExecuteCommand().";
                throw new std::runtime_error(aMessage.str());
            }
        }
        else if (cByte == ')')
        {
            std::string strCommandString = strCommand.str();

            if (strCommandString.empty() == true)
            {
                throw new std::runtime_error("Command started without name.");
            }

            std::list<std::unique_ptr<std::string>> aArguments;

            int nResult = ExecuteCommand(strCommandString, aArguments, pResult);

            if (nResult == 2)
            {
                return 0;
            }
            else
            {
                std::stringstream aMessage;
                aMessage << "Unexpected return value " << nResult << " from BacInterpreter::ExecuteCommand() for an implicit standby area name command call without arguments.";
                throw new std::runtime_error(aMessage.str());
            }
        }
        else
        {
            int nByte(cByte);
            std::stringstream aMessage;
            aMessage << "Unexpected character '" << cByte << "' (0x"
                     << std::hex << std::uppercase << std::right << std::setfill('0') << std::setw(2) << nByte
                     << ").";
            throw new std::runtime_error(aMessage.str());
        }

    } while (true);
}

int BacInterpreter::HandleAttribute(const char& cFirstByte,
                                    InputStreamInterface& aInputStream,
                                    std::unique_ptr<std::string>& pResult)
{
    if (pResult != nullptr)
    {
        throw new std::invalid_argument("BacInterpreter::HandleAttribute() with pResult != nullptr.");
    }

    if (cFirstByte == '#')
    {
        do
        {
            std::unique_ptr<std::string> pValue(nullptr);

            HandleCommand(aInputStream, pValue, false);

            if (pValue == nullptr)
            {
                throw new std::runtime_error("BacInterpreter::HandleCommand() returned with nullptr for its pResult parameter.");
            }

            if (pResult == nullptr)
            {
                pResult = std::move(pValue);
            }
            else
            {
                *pResult += *pValue;
            }

            char cByte = ConsumeWhitespace(aInputStream);

            if (cByte == ',')
            {
                return 0;
            }
            else if (cByte == ')')
            {
                return 1;
            }
            else if (cByte == '#')
            {
                continue;
            }
            else if (cByte == '\0')
            {
                return 2;
            }
            else
            {
                int nByte(cByte);
                std::stringstream aMessage;
                aMessage << "Unexpected character '" << cByte << "' (0x"
                         << std::hex << std::uppercase << std::right << std::setfill('0') << std::setw(2) << nByte
                         << ").";
                throw new std::runtime_error(aMessage.str());
            }

        } while (true);
    }
    else if (cFirstByte == '(')
    {
        std::stringstream strValue;
        unsigned long nParenthesisCount = 1UL;
        char cByte = '\0';

        do
        {
            aInputStream.get(cByte);

            if (aInputStream.eof() == true)
            {
                throw new std::runtime_error("Unexpected end of stream within reading an escaped argument value.");
            }

            if (aInputStream.bad() == true)
            {
                throw new std::runtime_error("Stream is bad.");
            }

            if (cByte == '(')
            {
                ++nParenthesisCount;
                strValue << cByte;
            }
            else if (cByte == ')')
            {
                --nParenthesisCount;

                if (nParenthesisCount == 0UL)
                {
                    pResult.reset(new std::string);
                    *pResult = strValue.str();

                    cByte = ConsumeWhitespace(aInputStream);

                    if (cByte == '\0')
                    {
                        throw new std::runtime_error("Unexpected end of stream while consuming whitespace after an escaped argument.");
                    }
                    else if (cByte == ',')
                    {
                        return 0;
                    }
                    else if (cByte == ')')
                    {
                        return 1;
                    }
                    else
                    {
                        int nByte(cByte);
                        std::stringstream aMessage;
                        aMessage << "Unexpected character '" << cByte << "' (0x"
                                 << std::hex << std::uppercase << std::right << std::setfill('0') << std::setw(2) << nByte
                                 << ").";
                        throw new std::runtime_error(aMessage.str());
                    }
                }
                else
                {
                    strValue << cByte;
                }
            }
            else
            {
                strValue << cByte;
            }

        } while (true);
    }

    std::stringstream strValue;

    char cByte = cFirstByte;

    do
    {
        if (cByte == ',')
        {
            pResult.reset(new std::string);
            *pResult = strValue.str();

            return 0;
        }
        else if (cByte == ')')
        {
            pResult.reset(new std::string);
            *pResult = strValue.str();

            return 1;
        }
        else
        {
            strValue << cByte;
        }

        aInputStream.get(cByte);

        if (aInputStream.eof() == true)
        {
            throw new std::runtime_error("Unexpected end of stream within reading an argument value.");
        }

        if (aInputStream.bad() == true)
        {
            throw new std::runtime_error("Stream is bad.");
        }

    } while (true);
}

int BacInterpreter::ExecuteCommand(const std::string& strCommand,
                                   const std::list<std::unique_ptr<std::string>>& aArguments,
                                   std::unique_ptr<std::string>& pResult)
{
    if (pResult != nullptr)
    {
        throw new std::invalid_argument("BacInterpreter::ExecuteCommand() with pResult != nullptr.");
    }

    if (strCommand == "CL")
    {
        if (aArguments.size() < 1)
        {
            throw new std::runtime_error("Command \"CL\" without at least one argument.");
        }

        std::list<std::unique_ptr<std::string>>::const_iterator iterArgument = aArguments.begin();

        if ((*iterArgument)->empty() == true)
        {
            throw new std::runtime_error("Command \"CL\" with an empty first argument.");
        }

        std::map<std::string, std::unique_ptr<Segment>>::const_iterator iterStandby = m_aStandby.find(**iterArgument);

        if (iterStandby != m_aStandby.end())
        {
            /**
             * @todo Copying is dumb and expensive! Some list processing instead?
             */

            std::vector<std::unique_ptr<std::string>> aStrings;

            iterArgument++;

            while (iterArgument != aArguments.end())
            {
                aStrings.push_back(std::unique_ptr<std::string>(new std::string(**iterArgument)));
                iterArgument++;
            }

            pResult.reset(new std::string);
            *pResult = iterStandby->second->GetString(aStrings);

            return 1;
        }
        else
        {
            std::stringstream aMessage;
            aMessage << "Unknown standby area name \"" << **iterArgument << "\".";
            throw new std::runtime_error(aMessage.str());
        }
    }
    else if (strCommand == "DS")
    {
        if (aArguments.size() != 2)
        {
            throw new std::runtime_error("Command \"DS\" without 2 arguments.");
        }

        std::list<std::unique_ptr<std::string>>::const_iterator iterArgument = aArguments.begin();

        if ((*iterArgument)->empty() == true)
        {
            throw new std::runtime_error("Command \"DS\" with an empty first argument.");
        }

        std::map<std::string, BacFunction>::const_iterator iterFunction = m_aFunctions.find(**iterArgument);

        if (iterFunction != m_aFunctions.end())
        {
            std::stringstream aMessage;
            aMessage << "Can't redefine internal, built-in command \"" << **iterArgument << "\".";
            throw new std::runtime_error(aMessage.str());
        }

        std::string strString(**iterArgument);
        std::string strValue(**(++iterArgument));

        std::map<std::string, std::unique_ptr<Segment>>::const_iterator iterStandby = m_aStandby.find(strString);

        if (iterStandby != m_aStandby.end())
        {
            iterStandby->second->SetString(strValue);
        }
        else
        {
            m_aStandby.insert(std::pair<std::string, std::unique_ptr<Segment>>(strString, std::unique_ptr<Segment>(new Segment(strValue))));
        }

        pResult.reset(new std::string);

        return 0;
    }
    else if (strCommand == "SS")
    {
        if (aArguments.size() < 2)
        {
            throw new std::runtime_error("Command \"SS\" with less than 2 arguments.");
        }

        std::list<std::unique_ptr<std::string>>::const_iterator iterArgument = aArguments.begin();

        if ((*iterArgument)->empty() == true)
        {
            throw new std::runtime_error("Command \"SS\" with an empty first argument.");
        }

        std::map<std::string, std::unique_ptr<Segment>>::const_iterator iterStandby = m_aStandby.find(**iterArgument);

        if (iterStandby == m_aStandby.end())
        {
            std::stringstream aMessage;
            aMessage << "Unknown standby area name \"" << **iterArgument << "\".";
            throw new std::runtime_error(aMessage.str());
        }

        iterArgument++;

        /**
         * @todo Copying is dumb and expensive! Some list processing instead?
         */

        std::list<std::unique_ptr<std::string>> aStrings;

        while (iterArgument != aArguments.end())
        {
            aStrings.push_back(std::unique_ptr<std::string>(new std::string(**iterArgument)));
            iterArgument++;
        }

        // If aArguments contains duplicates, the expectation is
        // that DrillHole() will fail on the second attempt for
        // not finding the drill segment any more.
        (*iterStandby).second->DrillHoles(aStrings);

        pResult.reset(new std::string);

        return 0;
    }
    else if (strCommand == "LN")
    {
        std::string strDelimiter;

        if (aArguments.size() > 0)
        {
            std::list<std::unique_ptr<std::string>>::const_iterator iterArgument = aArguments.begin();
            strDelimiter = **iterArgument;
        }

        std::stringstream strNames;

        for (std::map<std::string, std::unique_ptr<Segment>>::const_iterator iterStandby = m_aStandby.begin();
             iterStandby != m_aStandby.end();
             iterStandby++)
        {
            if (iterStandby != m_aStandby.begin())
            {
                strNames << strDelimiter;
            }

            strNames << iterStandby->first;
        }

        pResult.reset(new std::string(strNames.str()));

        if (aArguments.size() > 0)
        {
            return 0;
        }
        else
        {
            return 2;
        }
    }
    else if (strCommand == "DD")
    {
        if (aArguments.size() < 1)
        {
            throw new std::runtime_error("Command \"DD\" without at least one argument.");
        }

        for (std::list<std::unique_ptr<std::string>>::const_iterator iterArgument = aArguments.begin();
             iterArgument != aArguments.end();
             iterArgument++)
        {
            if (m_aStandby.erase(**iterArgument) < 1)
            {
                std::stringstream aMessage;
                aMessage << "Unknown standby area name \"" << **iterArgument << "\".";
                throw new std::runtime_error(aMessage.str());
            }
        }

        pResult.reset(new std::string);

        return 0;
    }
    else if (strCommand == "DA")
    {
        m_aStandby.clear();

        pResult.reset(new std::string);

        return 2;
    }
    else if (strCommand == "SB")
    {
        if (aArguments.size() < 2)
        {
            throw new std::runtime_error("Command \"SB\" with less than 2 arguments.");
        }

        std::list<std::unique_ptr<std::string>>::const_iterator iterBlockName = aArguments.begin();

        if ((*iterBlockName)->empty() == true)
        {
            throw new std::runtime_error("Command \"SB\" with an empty block name.");
        }


        /** @todo Prevent risky special characters in name */

        {
            std::ifstream aDisk(**iterBlockName + ".bac");

            if (aDisk.good() == true)
            {
                aDisk.close();

                throw new std::runtime_error("Won't overwrite \"" + **iterBlockName + "\" (\"" + **iterBlockName + ".bac\") on disk, is already existing.");
            }
        }

        std::unique_ptr<std::ofstream> pDisk = nullptr;
        // If SB would only save a single command, no special characters would
        // need to be reserved, and everything could be read in again as-is.
        // Now, in theory, there's a problem with escaping the literal use of
        // the character vs. being used in a special capacity/meaning as a delimiter.
        /** @todo Maybe these are a problem with consecutive bytes if source is UTF-8/UTF-16? */
        unsigned char cRecordSeparator(0x1E);
        unsigned char cUnitSeparator(0x1F);
        unsigned char cHoleDelimiter(0x1A);

        std::list<std::unique_ptr<std::string>>::const_iterator iterArgument = iterBlockName;
        ++iterArgument;

        for (;
             iterArgument != aArguments.end();
             iterArgument++)
        {
            if ((*iterArgument)->empty() == true)
            {
                throw new std::runtime_error("Command \"SB\" with an empty form name argument.");
            }

            std::map<std::string, BacFunction>::const_iterator iterFunction = m_aFunctions.find(**iterArgument);

            if (iterFunction != m_aFunctions.end())
            {
                std::stringstream aMessage;
                aMessage << "Can't store internal, built-in command \"" << **iterArgument << "\" to disk.";
                throw new std::runtime_error(aMessage.str());
            }

            std::map<std::string, std::unique_ptr<Segment>>::const_iterator iterStandby = m_aStandby.find(**iterArgument);

            if (iterStandby == m_aStandby.end())
            {
                std::stringstream aMessage;
                aMessage << "Unknown standby area name \"" << **iterArgument << "\".";
                throw new std::runtime_error(aMessage.str());
            }

            if (pDisk == nullptr)
            {
                pDisk = std::unique_ptr<std::ofstream>(new std::ofstream);

                pDisk->open(**iterBlockName + ".bac", std::ios::out | std::ios::trunc | std::ios::binary);

                if (pDisk->is_open() != true)
                {
                    throw new std::runtime_error("Can't open \"" + **iterBlockName + "\" (\"" + **iterBlockName + ".bac\") on disk for writing.");
                }
            }
            else
            {
                *pDisk << cRecordSeparator;
            }

            *pDisk << iterStandby->first << cUnitSeparator;

            std::string strString = iterStandby->second->GetStringOriginal();
            std::unique_ptr<std::map<std::size_t, Segment::Hole>> pHoles;

            if (iterStandby->second->GetHoles(pHoles) == 0)
            {
                std::size_t nStart = 0U;

                for (std::map<std::size_t, Segment::Hole>::iterator iterHole = pHoles->begin();
                    iterHole != pHoles->end();
                    iterHole++)
                {
                    *pDisk << strString.substr(nStart, iterHole->first - nStart)
                           << cHoleDelimiter << iterHole->second.GetHoleNumber()
                           << cHoleDelimiter << strString.substr(iterHole->first, iterHole->second.GetLength())
                           << cHoleDelimiter;

                    nStart = iterHole->first + iterHole->second.GetLength();
                }

                *pDisk << strString.substr(nStart, std::string::npos);
            }
            else
            {
                *pDisk << strString;
            }
        }

        if (pDisk != nullptr)
        {
            pDisk->close();
        }

        pResult.reset(new std::string);

        return 0;
    }
    else if (strCommand == "FB")
    {
        if (aArguments.size() != 1)
        {
            throw new std::runtime_error("Command \"FB\" without 1 argument.");
        }

        std::list<std::unique_ptr<std::string>>::const_iterator iterBlockName = aArguments.begin();

        if ((*iterBlockName)->empty() == true)
        {
            throw new std::runtime_error("Command \"FB\" with an empty block name.");
        }


        /** @todo Prevent risky special characters in name */

        std::ifstream aDisk;
        aDisk.open(**iterBlockName + ".bac", std::ios::in | std::ios::binary);

        if (aDisk.is_open() != true)
        {
            throw new std::runtime_error("Can't read \"" + **iterBlockName + "\" (\"" + **iterBlockName + ".bac\") from disk.");
        }

        std::string strBlock;

        {
            std::stringstream aBlock;
            char cBytes[1025] = { '\0' };

            do
            {
                aDisk.read(&cBytes[0], 1024);
                cBytes[aDisk.gcount()] = '\0';

                if (aDisk.eof() == true)
                {
                    break;
                }

                if (aDisk.bad() == true)
                {
                    throw new std::runtime_error("Stream is bad.");
                }

                aBlock << cBytes;

            } while (true);

            aDisk.close();

            aBlock << cBytes;
            strBlock = aBlock.str();
        }

        // If FB would only load a single command, no special characters would
        // need to be reserved, and everything could be read in again as-is.
        // Now, in theory, there's a problem with escaping the literal use of
        // the character vs. being used in a special capacity/meaning as a delimiter.
        /** @todo Maybe these are a problem with consecutive bytes if source is UTF-8/UTF-16? */
        unsigned char cRecordSeparator(0x1E);
        unsigned char cUnitSeparator(0x1F);
        unsigned char cHoleDelimiter(0x1A);
        std::size_t nPosRecordStart = 0;
        std::size_t nPosRecordEnd = 0;

        do
        {
            nPosRecordEnd = strBlock.find(cRecordSeparator, nPosRecordStart);

            std::string strRecord(strBlock.substr(nPosRecordStart, nPosRecordEnd));

            {
                std::size_t nPosUnit = strRecord.find(cUnitSeparator, 0);

                if (nPosUnit == std::string::npos)
                {
                    throw new std::runtime_error("Record without unit separator.");
                }

                std::string strCommand(strRecord.substr(0, nPosUnit));
                std::string strSequence(strRecord.substr(nPosUnit + 1, std::string::npos));

                {
                    std::map<std::string, BacFunction>::const_iterator iterFunction = m_aFunctions.find(strCommand);

                    if (iterFunction != m_aFunctions.end())
                    {
                        std::stringstream aMessage;
                        aMessage << "Can't overwrite internal, built-in command \"" << strCommand << "\" from disk.";
                        throw new std::runtime_error(aMessage.str());
                    }

                    std::map<std::string, std::unique_ptr<Segment>>::const_iterator iterStandby = m_aStandby.find(strCommand);

                    if (iterStandby != m_aStandby.end())
                    {
                        std::stringstream aMessage;
                        aMessage << "Won't overwrite existing standby area \"" << iterStandby->first << "\".";
                        throw new std::runtime_error(aMessage.str());
                    }
                }

                std::unique_ptr<Segment> pSegment;

                {
                    std::size_t nPosHole = strSequence.find(cHoleDelimiter);

                    if (nPosHole != std::string::npos)
                    {
                        std::string strString;
                        std::size_t nPosStart = 0U;
                        std::unique_ptr<std::map<std::size_t, Segment::Hole>> pHoles(new std::map<std::size_t, Segment::Hole>);

                        do
                        {
                            strString += strSequence.substr(nPosStart, nPosHole - nPosStart);
                            nPosStart = nPosHole + 1U;

                            nPosHole = strSequence.find(cHoleDelimiter, nPosStart);

                            if (nPosHole == std::string::npos)
                            {
                                throw new std::runtime_error("Hole delimiter not found.");
                            }

                            std::string strHoleNumber = strSequence.substr(nPosStart, nPosHole - nPosStart);
                            nPosStart = nPosHole + 1U;

                            /** @todo Currently no checks on validity of nHoleNumber: not
                              * negative, numbers all need to be consecutive (no gaps). */
                            long nHoleNumber = ParseNumber(strHoleNumber);

                            nPosHole = strSequence.find(cHoleDelimiter, nPosStart);

                            if (nPosHole == std::string::npos)
                            {
                                throw new std::runtime_error("Hole delimiter not found.");
                            }

                            std::string strHoleString = strSequence.substr(nPosStart, nPosHole - nPosStart);
                            nPosStart = nPosHole + 1U;

                            if (strHoleString.length() <= 0)
                            {
                                throw new std::runtime_error("Empty hole string.");
                            }

                            if (pHoles->insert(std::pair<std::size_t, Segment::Hole>(strString.length(),
                                                                                     Segment::Hole(strString.length(),
                                                                                                   strHoleString.length(),
                                                                                                   nHoleNumber))).second != true)
                            {
                                // This should be impossible, per the processing/constraints above.
                                // Also, logically, it's not allowed to have two holes start at the
                                // same position of the original string.

                                throw new std::runtime_error("Two holes at the same start position in the original sequence string.");
                            }

                            strString += strHoleString;

                            nPosHole = strSequence.find(cHoleDelimiter, nPosStart);

                        } while (nPosHole != std::string::npos);

                        strString += strSequence.substr(nPosStart, std::string::npos);

                        pSegment.reset(new Segment(strString, pHoles));
                    }
                    else
                    {
                        pSegment.reset(new Segment(strSequence));
                    }
                }

                m_aStandby.insert(std::pair<std::string, std::unique_ptr<Segment>>(strCommand, std::move(pSegment)));
            }

            if (nPosRecordEnd == std::string::npos)
            {
                break;
            }

            nPosRecordStart = nPosRecordEnd + 1;

        } while (true);

        pResult.reset(new std::string);

        return 0;
    }
    else if (strCommand == "CR")
    {
        if (aArguments.size() != 1)
        {
            throw new std::runtime_error("Command \"CR\" without 1 argument.");
        }

        std::list<std::unique_ptr<std::string>>::const_iterator iterArgument = aArguments.begin();

        if ((*iterArgument)->empty() == true)
        {
            throw new std::runtime_error("Command \"CR\" with an empty first argument.");
        }

        std::map<std::string, std::unique_ptr<Segment>>::const_iterator iterStandby = m_aStandby.find(**iterArgument);

        if (iterStandby != m_aStandby.end())
        {
            iterStandby->second->CallRestore();

            pResult.reset(new std::string);

            return 0;
        }
        else
        {
            std::stringstream aMessage;
            aMessage << "Unknown standby area name \"" << **iterArgument << "\".";
            throw new std::runtime_error(aMessage.str());
        }
    }
    else if (strCommand == "CN")
    {
        if (aArguments.size() != 3)
        {
            throw new std::runtime_error("Command \"CN\" without 3 arguments.");
        }

        std::list<std::unique_ptr<std::string>>::const_iterator iterArgument = aArguments.begin();

        if ((*iterArgument)->empty() == true)
        {
            throw new std::runtime_error("Command \"CN\" with an empty first argument.");
        }

        std::map<std::string, std::unique_ptr<Segment>>::const_iterator iterStandby = m_aStandby.find(**iterArgument);

        if (iterStandby == m_aStandby.end())
        {
            std::stringstream aMessage;
            aMessage << "Unknown standby area name \"" << **iterArgument << "\".";
            throw new std::runtime_error(aMessage.str());
        }

        iterArgument++;

        std::size_t nCharacterCount = 0U;

        {
            std::stringstream aConverter;
            aConverter << **iterArgument;
            // If negative: std::size_t is unsigned, leads to overflow.
            // Zero doesn't make sense either.
            aConverter >> nCharacterCount;

            if (aConverter.fail() != false)
            {
                std::stringstream aMessage;
                aMessage << "Converting \"" << **iterArgument << "\" to a number failed.";
                throw new std::runtime_error(aMessage.str());
            }
        }

        int nResult = iterStandby->second->CallN(nCharacterCount, pResult);

        if (nResult == 0)
        {
            return 1;
        }
        else if (nResult == 1)
        {
            iterArgument++;

            pResult.reset(new std::string);
            *pResult = **iterArgument;

            return 1;
        }
        else
        {
            std::stringstream aMessage;
            aMessage << "Unexpected return value " << nResult << " from Segment::CallN().";
            throw new std::runtime_error(aMessage.str());
        }

        return 0;
    }
    else if (strCommand == "CS")
    {
        if (aArguments.size() != 2)
        {
            throw new std::runtime_error("Command \"CS\" without 2 arguments.");
        }

        std::list<std::unique_ptr<std::string>>::const_iterator iterArgument = aArguments.begin();

        if ((*iterArgument)->empty() == true)
        {
            throw new std::runtime_error("Command \"CS\" with an empty first argument.");
        }

        std::map<std::string, std::unique_ptr<Segment>>::const_iterator iterStandby = m_aStandby.find(**iterArgument);

        if (iterStandby == m_aStandby.end())
        {
            std::stringstream aMessage;
            aMessage << "Unknown standby area name \"" << **iterArgument << "\".";
            throw new std::runtime_error(aMessage.str());
        }

        int nResult = iterStandby->second->CallSegment(pResult);

        if (nResult == 0)
        {
            return 1;
        }
        else if (nResult == 1)
        {
            iterArgument++;

            pResult.reset(new std::string);
            *pResult = **iterArgument;

            return 1;
        }
        else
        {
            std::stringstream aMessage;
            aMessage << "Unexpected return value " << nResult << " from Segment::CallSegment().";
            throw new std::runtime_error(aMessage.str());
        }

        return 0;
    }
    else if (strCommand == "CC")
    {
        if (aArguments.size() != 2)
        {
            throw new std::runtime_error("Command \"CC\" without 2 arguments.");
        }

        std::list<std::unique_ptr<std::string>>::const_iterator iterArgument = aArguments.begin();

        if ((*iterArgument)->empty() == true)
        {
            throw new std::runtime_error("Command \"CC\" with an empty first argument.");
        }

        std::map<std::string, std::unique_ptr<Segment>>::const_iterator iterStandby = m_aStandby.find(**iterArgument);

        if (iterStandby == m_aStandby.end())
        {
            std::stringstream aMessage;
            aMessage << "Unknown standby area name \"" << **iterArgument << "\".";
            throw new std::runtime_error(aMessage.str());
        }

        int nResult = iterStandby->second->CallCharacter(pResult);

        if (nResult == 0)
        {
            return 0;
        }
        else if (nResult == 1)
        {
            iterArgument++;

            pResult.reset(new std::string);
            *pResult = **iterArgument;

            return 0;
        }
        else
        {
            std::stringstream aMessage;
            aMessage << "Unexpected return value " << nResult << " from Segment::CallCharacter().";
            throw new std::runtime_error(aMessage.str());
        }

        return 0;
    }
    else if (strCommand == "IN")
    {
        if (aArguments.size() != 3)
        {
            throw new std::runtime_error("Command \"IN\" without 3 arguments.");
        }

        std::list<std::unique_ptr<std::string>>::const_iterator iterArgument = aArguments.begin();

        if ((*iterArgument)->empty() == true)
        {
            throw new std::runtime_error("Command \"IN\" with an empty first argument.");
        }

        std::map<std::string, std::unique_ptr<Segment>>::const_iterator iterStandby = m_aStandby.find(**iterArgument);

        if (iterStandby == m_aStandby.end())
        {
            std::stringstream aMessage;
            aMessage << "Unknown standby area name \"" << **iterArgument << "\".";
            throw new std::runtime_error(aMessage.str());
        }

        iterArgument++;

        if ((*iterArgument)->empty() == true)
        {
            throw new std::runtime_error("Command \"IN\" with an empty second argument.");
        }

        int nResult = iterStandby->second->CallIn(**iterArgument, pResult);

        if (nResult == 0)
        {
            pResult.reset(new std::string);

            return 0;
        }
        else if (nResult == 1)
        {
            iterArgument++;

            pResult.reset(new std::string);
            *pResult = **iterArgument;

            return 0;
        }
        else
        {
            std::stringstream aMessage;
            aMessage << "Unexpected return value " << nResult << " from Segment::CallIn().";
            throw new std::runtime_error(aMessage.str());
        }

        return 0;
    }
    else if (strCommand == "CM")
    {
        if (aArguments.size() != 1)
        {
            throw new std::runtime_error("Command \"CM\" without 1 argument.");
        }

        std::list<std::unique_ptr<std::string>>::const_iterator iterArgument = aArguments.begin();

        if ((*iterArgument)->empty() == true)
        {
            throw new std::runtime_error("Command \"CM\" with an empty first argument.");
        }

        m_cMetaCharacter = (*iterArgument)->at(0);

        pResult.reset(new std::string);

        return 0;
    }
    else if (strCommand == "RS")
    {
        std::stringstream strInput;
        char cByte = '\0';

        do
        {
            m_aInputStream.get(cByte);

            if (m_aInputStream.eof() == true)
            {
                throw new std::runtime_error("Unexpected end of stream during \"RS\" command.");
            }

            if (m_aInputStream.bad() == true)
            {
                throw new std::runtime_error("Stream is bad.");
            }

            if (cByte == m_cMetaCharacter)
            {
                break;
            }

            strInput << cByte;

        } while (true);

        pResult.reset(new std::string);
        *pResult = strInput.str();

        return 2;
    }
    else if (strCommand == "RC")
    {
        char cByte = '\0';

        m_aInputStream.get(cByte);

        if (m_aInputStream.eof() == true)
        {
            throw new std::runtime_error("Unexpected end of stream during \"RC\" command.");
        }

        if (m_aInputStream.bad() == true)
        {
            throw new std::runtime_error("Stream is bad.");
        }

        pResult.reset(new std::string);
        *pResult += cByte;

        return 2;
    }
    else if (strCommand == "PS")
    {
        if (aArguments.size() != 1)
        {
            throw new std::runtime_error("Command \"PS\" without 1 argument.");
        }

        std::list<std::unique_ptr<std::string>>::const_iterator iterArgument = aArguments.begin();

        if ((*iterArgument)->empty() == true)
        {
            throw new std::runtime_error("Command \"PS\" with an empty first argument.");
        }

        m_aOutputStream.print(**iterArgument);

        pResult.reset(new std::string);

        return 0;
    }
    else
    {
        std::map<std::string, BacFunction>::iterator iter = m_aFunctions.find(strCommand);

        if (iter != m_aFunctions.end())
        {
            iter->second(aArguments, pResult);
            return 0;
        }

        {
            // This can be a security problem if implicit command calls into
            // standby area name resolving ends up replacing a regular function
            // that's expected to be there as an built-in command, but isn't.
            // At least, currently all built-in functions require at least one
            // argument, and this mechanism here doesn't allow any arguments.

            std::map<std::string, std::unique_ptr<Segment>>::const_iterator iterStandby = m_aStandby.find(strCommand);

            if (iterStandby != m_aStandby.end())
            {
                if (aArguments.size() > 0)
                {
                    std::stringstream aMessage;
                    aMessage << "Implicit standby area name \"" << strCommand << "\" unexpectedly followed by an argument.";
                    throw new std::runtime_error(aMessage.str());
                }

                pResult.reset(new std::string);
                *pResult = iterStandby->second->GetString(std::vector<std::unique_ptr<std::string>>());

                if (pResult->empty() == true)
                {
                    return 2;
                }

                if (pResult->at(0) != '#')
                {
                    return 2;
                }

                InputStreamString aAttribute(pResult->substr(1));

                std::unique_ptr<std::string> pValue(nullptr);

                int nAttributeResult = HandleAttribute(pResult->at(0), aAttribute, pValue);

                if (nAttributeResult == 0)
                {
                    throw new std::runtime_error("Use of BacInterpreter::HandleAttribute() to call a command failed.");
                }
                else if (nAttributeResult == 1)
                {
                    pResult = std::move(pValue);
                    return 2;
                }
                else if (nAttributeResult == 2)
                {
                    // Was a call, pseudo "attribute" handling.
                    pResult = std::move(pValue);
                    return 2;
                }
                else
                {
                    std::stringstream aMessage;
                    aMessage << "Unexpected return value " << nAttributeResult << " from BacInterpreter::HandleAttribute().";
                    throw new std::runtime_error(aMessage.str());
                }
            }
        }

        std::stringstream aMessage;
        aMessage << "Unknown command \"" << strCommand << "\".";
        throw new std::runtime_error(aMessage.str());
    }
}

/**
 * @retval Returns the first non-whitespace character or '\0' in
 *     case of end-of-file.
 */
char BacInterpreter::ConsumeWhitespace(InputStreamInterface& aInputStream)
{
    char cByte('\0');

    do
    {
        aInputStream.get(cByte);

        if (aInputStream.eof() == true)
        {
            return '\0';
        }

        if (aInputStream.bad() == true)
        {
            throw new std::runtime_error("Stream is bad.");
        }

        if (std::isspace(cByte, m_aLocale) == 0)
        {
            return cByte;
        }

    } while (true);
}

}
