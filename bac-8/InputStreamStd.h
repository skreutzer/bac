/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of BAC.
 *
 * BAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * BAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with BAC. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/InputStreamStd.h
 * @author Stephan Kreutzer
 * @since 2021-06-14
 */

#ifndef _BAC_INPUTSTREAMSTD_H
#define _BAC_INPUTSTREAMSTD_H

#include "InputStreamInterface.h"
#include <istream>

namespace bac
{

class InputStreamStd : public InputStreamInterface
{
public:
    InputStreamStd(std::istream& aStream);
    ~InputStreamStd();

public:
    int get(char& cCharacter);
    bool eof();
    bool bad();

protected:
    std::istream& m_aStream;

};

}

#endif
