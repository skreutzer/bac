/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of BAC.
 *
 * BAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * BAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with BAC. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/OutputStreamCurses.h
 * @author Stephan Kreutzer
 * @since 2021-06-19
 */

#ifndef _BAC_OUTPUTSTREAMCURSES_H
#define _BAC_OUTPUTSTREAMCURSES_H

#include "OutputStreamInterface.h"

namespace bac
{

class OutputStreamCurses : public OutputStreamInterface
{
public:
    /** @todo Take SCREEN* as argument? */
    OutputStreamCurses();
    ~OutputStreamCurses();

public:
    int print(const std::string& str);

};

}

#endif
