/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of BAC.
 *
 * BAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * BAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with BAC. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/InputStreamStd.cpp
 * @author Stephan Kreutzer
 * @since 2021-06-14
 */


#include "InputStreamStd.h"


namespace bac
{

InputStreamStd::InputStreamStd(std::istream& aStream):
  m_aStream(aStream)
{

}

InputStreamStd::~InputStreamStd()
{

}

int InputStreamStd::get(char& cCharacter)
{
    m_aStream.get(cCharacter);

    return 0;
}

bool InputStreamStd::eof()
{
    return m_aStream.eof();
}

bool InputStreamStd::bad()
{
    return m_aStream.bad();
}

}
