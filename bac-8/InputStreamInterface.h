/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of BAC.
 *
 * BAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * BAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with BAC. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/InputStreamInterface.h
 * @author Stephan Kreutzer
 * @since 2021-06-13
 */

#ifndef _BAC_INPUTSTREAMINTERFACE_H
#define _BAC_INPUTSTREAMINTERFACE_H

namespace bac
{

class InputStreamInterface
{
public:
    virtual int get(char& cCharacter) = 0;
    virtual bool eof() = 0;
    virtual bool bad() = 0;

};

}

#endif
