/* Copyright (C) 2020-2021 Stephan Kreutzer
 *
 * This file is part of BAC.
 *
 * BAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * BAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with BAC. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/BacImplementation.h
 * @author Stephan Kreutzer
 * @since 2020-09-24
 */

#ifndef _BACIMPLEMENTATION_H
#define _BACIMPLEMENTATION_H

#include <list>
#include <memory>
#include <string>

int AD(const std::list<std::unique_ptr<std::string>>& aArguments, std::unique_ptr<std::string>& pResult);
int SU(const std::list<std::unique_ptr<std::string>>& aArguments, std::unique_ptr<std::string>& pResult);
int ML(const std::list<std::unique_ptr<std::string>>& aArguments, std::unique_ptr<std::string>& pResult);
int DV(const std::list<std::unique_ptr<std::string>>& aArguments, std::unique_ptr<std::string>& pResult);
int PS(const std::list<std::unique_ptr<std::string>>& aArguments, std::unique_ptr<std::string>& pResult);

#endif
